## cat, but in asm

Mimics the UNIX utility `cat`, with the exception that this uses `read(2)` and
`write(2)` only when no file is given. Otherwise, the file's contents are
`mmap(2)`ed and written to stdout.

### How to Compile?
To produce the executable, run:
```sh
make
```

### Usage
```
./cat <file to read>
```
If no file is given, then `cat` will read from `stdin`.
Only one file will be handled for now.
