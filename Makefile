.PHONY: all

all:
	cc -Wl,-z,relro,-z,now -nostdlib -static-pie -o cat cat.S
	strip ./cat
